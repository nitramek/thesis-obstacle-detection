#ifndef OBSTACLE_DETECTION_NODE_H_
#define OBSTACLE_DETECTION_NODE_H_

#include "ros/ros.h"
#include "Eigen/Eigen"
#include "sensor_msgs/PointCloud2.h"
#include "cartographer_ros_msgs/SubmapEntry.h"
#include "cartographer_ros_msgs/SubmapList.h"
#include "cartographer_ros_msgs/SubmapCloudQuery.h"

#include "obstacle_detection/sub_octo_maps.h"
#include "obstacle_detection/object_checker.h"
#include "ros/callback_queue_interface.h"

namespace obstacle_detection
{
class Node
{
private:
  ros::Subscriber submaps_subscriber_;
  ros::ServiceClient submap_cloud_query_client_;

  std::shared_ptr<SubOctoMaps> sub_octo_maps_;
  std::shared_ptr<ObjectChecker> checker_;

  void submapsCallback(const cartographer_ros_msgs::SubmapList::ConstPtr& msg);

  cartographer_ros_msgs::SubmapCloudQuery::Response fetchSubmapResponse(const int trajectory_id,
                                                                        const int submap_index);
  bool initiated_;
public:
  Node(ros::NodeHandle& nodeHandle, ros::NodeHandle& private_node, const tf2_ros::Buffer* const tf_buffer);
  virtual ~Node();
};  // class Node

class ResponseCallback : public ros::CallbackInterface
{
private:
  cartographer_ros_msgs::SubmapEntry entry_;
  ros::ServiceClient& submap_cloud_query_client_;
  SubOctoMaps& sub_octo_maps_;

public:
  ResponseCallback(cartographer_ros_msgs::SubmapEntry& entry, ros::ServiceClient& submap_cloud_query_client,
                   SubOctoMaps& sub_octo_maps)
    : entry_(entry), submap_cloud_query_client_(submap_cloud_query_client), sub_octo_maps_(sub_octo_maps)
  {
  }
  virtual CallResult call()
  {
    cartographer_ros_msgs::SubmapCloudQuery scq;
    scq.request.high_resolution = true;
    scq.request.trajectory_id = entry_.trajectory_id;
    scq.request.submap_index = entry_.submap_index;
    scq.request.min_probability = 0.8;
    if (submap_cloud_query_client_.call(scq))
    {
      sub_octo_maps_.handleSubmapQueryResult(scq, entry_);
      return CallResult::Success;
    }
    else
    {
      ROS_WARN("Cartographer submap cloud query for [trajectory_id: %d, submap_index: %d] wasnt completed",
               scq.request.trajectory_id, scq.request.submap_index);
      return CallResult::TryAgain;
    }
  }
};
}  // namespace obstacle_detection
#endif  // OBSTACLE_DETECTION_NODE_H_