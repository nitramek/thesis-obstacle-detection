
#ifndef OBSTACLE_DETECTION_ROS_HELPERS_H_
#define OBSTACLE_DETECTION_ROS_HELPERS_H_
#include <string>

#include "Eigen/Core"
#include "urdf/model.h"

namespace obstacle_detection
{
Eigen::Vector3d getCarDimension()
{
  urdf::Model model;
  model.initParam("/robot_description");
  const std::string linkName = std::string("base_link");
  boost::shared_ptr<const urdf::Link> link = model.getLink(linkName);
  boost::shared_ptr<urdf::Box> box = boost::dynamic_pointer_cast<urdf::Box>(link->visual->geometry);
  Eigen::Vector3d dim(box->dim.x, box->dim.y, box->dim.z);
  return dim;
}
}
#define LAZY_PUBLISH(pub, msg){\
      if (pub.getNumSubscribers() > 0)\
      {\
        pub.publish(msg);\
      }\
}
#endif