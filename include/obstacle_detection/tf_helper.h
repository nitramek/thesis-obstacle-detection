#ifndef OBSTACLE_DETECTION_TF_HELPER_H
#include <memory>
#include <unordered_map>
#include <mutex>

#include "tf2_ros/transform_listener.h"
#include "Eigen/Eigen"
#include "geometry_msgs/TransformStamped.h"
#include "tf/transform_broadcaster.h"
#include "obstacle_detection/conversions.h"

namespace obstacle_detection
{
class TFHelper
{
public:
  Eigen::Affine3d lookupTransform(const std::string& sourceFrameId, const std::string& targetFrameId,
                                  int waitDuration = 0);

  TFHelper(ros::NodeHandle& n, const tf2_ros::Buffer* const tf_buffer);
  /**
   *  Registres transform for periodic updating, if there is one it is updated
   **/
  void registerTransform(int id, geometry_msgs::TransformStamped transform);

private:
  std::unordered_map<int, geometry_msgs::TransformStamped> registered_transforms_;
  void broadcast(const ros::WallTimerEvent& event);
  const tf2_ros::Buffer* const tf_buffer_;
  std::mutex mutex_;
  tf::TransformBroadcaster tf2_broadcaster_;
  ros::WallTimer broadcast_timer_;
};  // class TFHelper

inline void TFHelper::broadcast(const ros::WallTimerEvent& event)
{
  std::lock_guard<std::mutex> lock(mutex_);
  for (auto& registeredTransformPair : registered_transforms_)
  {
    registeredTransformPair.second.header.stamp = ros::Time::now();
    tf2_broadcaster_.sendTransform(registeredTransformPair.second);
  }
}

inline void TFHelper::registerTransform(int id, geometry_msgs::TransformStamped transform)
{
  std::lock_guard<std::mutex> lock(mutex_);
  registered_transforms_[id] = transform;
}
inline Eigen::Affine3d TFHelper::lookupTransform(const std::string& sourceFrameId, const std::string& targetFrameId,
                                                 int waitDuration)
{
  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped =
        tf_buffer_->lookupTransform(targetFrameId, sourceFrameId, ros::Time(0), ros::Duration(waitDuration));
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    try
    {
      transformStamped =
          tf_buffer_->lookupTransform(targetFrameId, sourceFrameId, ros::Time(0), ros::Duration(waitDuration));
    }
    catch (tf2::TransformException& ex)
    {
      ROS_ERROR("%s", ex.what());
      return Eigen::Affine3d();
    }
  }
  Eigen::Affine3d affineTransform;
  tf::transformMsgToEigen(transformStamped.transform, affineTransform);
  return affineTransform;
}

inline TFHelper::TFHelper(ros::NodeHandle& n, const tf2_ros::Buffer* const tf_buffer) : tf_buffer_(tf_buffer)
{
  broadcast_timer_ = n.createWallTimer(ros::WallDuration(0.01), &TFHelper::broadcast, this);
}
}
#endif  // !OBSTACLE_DETECTION_TF_HELPER_H