#ifndef OBSTACLE_DETECTION_CONVERSIONS_H_
#define OBSTACLE_DETECTION_CONVERSIONS_H_

#include "pcl/point_types.h"
#include "octomap/Pointcloud.h"
#include "octomap_ros/conversions.h"
#include "pcl_conversions/pcl_conversions.h"
#include "pcl/PCLPointCloud2.h"
#include "pcl/conversions.h"
#include "octomap_msgs/Octomap.h"
#include "octomap_msgs/conversions.h"
#include "ros/console.h"
namespace obstacle_detection
{
pcl::PointCloud<pcl::PointXYZI>::Ptr msgToPcl(sensor_msgs::PointCloud2& msg);
sensor_msgs::PointCloud2 pclToMsg(pcl::PointCloud<pcl::PointXYZI>::Ptr pclCloud);
octomap::Pointcloud msgToOctomap(sensor_msgs::PointCloud2& msg);
geometry_msgs::TransformStamped createTransformStampedFromPose(const geometry_msgs::Pose& pose,
                                                               const std::string& frameId,
                                                               const std::string& childFrameId);
geometry_msgs::Transform createTransformFromPose(const geometry_msgs::Pose& pose);

}  // namespace obstacle_detection

inline pcl::PointCloud<pcl::PointXYZI>::Ptr obstacle_detection::msgToPcl(sensor_msgs::PointCloud2& msg)
{
  pcl::PCLPointCloud2 pcl_pc2;
  pcl_conversions::toPCL(msg, pcl_pc2);
  pcl::PointCloud<pcl::PointXYZI>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::fromPCLPointCloud2(pcl_pc2, *temp_cloud);
  return temp_cloud;
}

inline sensor_msgs::PointCloud2 obstacle_detection::pclToMsg(pcl::PointCloud<pcl::PointXYZI>::Ptr pclCloud)
{
  pcl::PCLPointCloud2 pcl_pc2;
  pcl::toPCLPointCloud2(*pclCloud, pcl_pc2);
  sensor_msgs::PointCloud2 msg;
  pcl_conversions::fromPCL(pcl_pc2, msg);
  return msg;
}

inline geometry_msgs::Transform obstacle_detection::createTransformFromPose(const geometry_msgs::Pose& pose){
  geometry_msgs::Transform transform;
  transform.translation.x = pose.position.x;
  transform.translation.y = pose.position.y;
  transform.translation.z = pose.position.z;
  transform.rotation.x = pose.orientation.x;
  transform.rotation.y = pose.orientation.y;
  transform.rotation.z = pose.orientation.z;
  transform.rotation.w = pose.orientation.w;
  return transform;
}

inline geometry_msgs::TransformStamped obstacle_detection::createTransformStampedFromPose(
    const geometry_msgs::Pose& pose, const std::string& frameId, const std::string& childFrameId)
{
  geometry_msgs::TransformStamped transform;
  transform.header.stamp = ros::Time::now();
  transform.header.frame_id = "map";
  transform.child_frame_id = "active_octomap_frame";
  transform.transform = createTransformFromPose(pose);

  return transform;
}
#endif  // OBSTACLE_DETECTION_CONVERSIONS_H_