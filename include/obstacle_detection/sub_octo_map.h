#ifndef OBSTACLE_DETECTION_SUB_OCTO_MAP_H_
#define OBSTACLE_DETECTION_SUB_OCTO_MAP_H_

#include <mutex>

#include "octomap/octomap.h"
#include "octomap_msgs/Octomap.h"
#include "sensor_msgs/PointCloud2.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/TransformStamped.h"
#include "Eigen/Eigen"
#include "eigen_conversions/eigen_msg.h"
#include "tf_conversions/tf_eigen.h"

#include <pcl_ros/point_cloud.h>
#include <pcl/octree/octree_search.h>

namespace obstacle_detection
{
class SubOctoMap
{
public:
  SubOctoMap();
  SubOctoMap(int trajectory_id, int submap_index, float resolution, geometry_msgs::Pose pose);
  void update(sensor_msgs::PointCloud2& cloud_msg, int version, bool finished);
  bool rayCollision(Eigen::Vector3d& origin, Eigen::Vector3d& direction, Eigen::Vector3d& end, float maxRange = -1.f);
  pcl::PointCloud<pcl::PointXYZI>::Ptr mapCopy();
  bool finished() const
  {
    return finished_;
  }
  void set_pose(geometry_msgs::Pose pose)
  {
    std::lock_guard<std::mutex> lock(mutex_);
    pose_ = pose;
    tf::poseMsgToEigen(pose_, from_submap_transform_);
    to_submap_transform_ = from_submap_transform_.inverse();
    tf::transformEigenToMsg(to_submap_transform_, transform_.transform);
  }
  const geometry_msgs::Pose& pose() const
  {
    return pose_;
  }
  float resolution() const
  {
    return resolution_;
  }
  int submap_index() const
  {
    return submap_index_;
  }

private:
  std::mutex mutex_;
  int trajectory_id_;
  int submap_index_;
  int version_;
  float resolution_;  // in meters
  bool finished_;
  geometry_msgs::Pose pose_;
  Eigen::Affine3d from_submap_transform_;
  Eigen::Affine3d to_submap_transform_;
  geometry_msgs::TransformStamped transform_;

  pcl::PointCloud<pcl::PointXYZI>::Ptr map_data_;
  pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>::Ptr map_octree_;
};  // class SubOctoMap
}  // namespace obstacle_detection

#endif  // OBSTACLE_DETECTION_SUB_OCTO_MAP_H_
