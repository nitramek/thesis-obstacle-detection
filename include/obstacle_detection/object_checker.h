#ifndef OBSTACLE_DETECTION_OBJECT_CHECKER_H_
#define OBSTACLE_DETECTION_OBJECT_CHECKER_H_
#include <thread>

#include "ros/ros.h"
#include "obstacle_detection/sub_octo_maps.h"
#include "tf/transform_broadcaster.h"
#include "visualization_msgs/MarkerArray.h"
#include "obstacle_detection/tf_helper.h"

namespace obstacle_detection
{
class ObjectChecker
{
public:
  explicit ObjectChecker(SubOctoMaps& maps, ros::NodeHandle& n, ros::NodeHandle& private_node, const tf2_ros::Buffer* const tf_buffer);
  void start();
  void checkCurrentMap();
  void registerTransformForOctomap(SubOctoMap& octomap);
  ~ObjectChecker();

private:
  float max_range_;
  float detection_rate_hz_;
  Eigen::Vector3d car_dimension_;
  std::vector<std::pair<Eigen::Vector3d, Eigen::Vector3d>> rays_;
  visualization_msgs::MarkerArray ray_markers_;
  std::thread checker_thread_;
  SubOctoMaps& maps_;
  ros::Publisher active_octomap_pub_;
  ros::Publisher casted_rays_pub_;
  tf::TransformBroadcaster tf2_broadcaster_;
  Eigen::Affine3d car_position_transform_;
  TFHelper tf_helper_;
};  // class ObjectChecker
}  // namespace obstacle_detection

#endif  // OBSTACLE_DETECTION_OBJECT_CHECKER_H_
