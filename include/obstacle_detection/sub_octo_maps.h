#ifndef OBSTACLE_DETECTION_SUB_OCTO_MAPS_H_
#define OBSTACLE_DETECTION_SUB_OCTO_MAPS_H_
#include <vector>
#include <map>
#include <functional>
#include <mutex>
#include "cartographer_ros_msgs/SubmapEntry.h"
#include "cartographer_ros_msgs/SubmapList.h"
#include "cartographer_ros_msgs/SubmapCloudQuery.h"
#include "sensor_msgs/PointCloud2.h"
#include "obstacle_detection/sub_octo_map.h"

namespace obstacle_detection
{
struct SubmapId
{
  int trajectory_id;
  int submap_index;

  bool operator==(const SubmapId& other) const
  {
    return std::forward_as_tuple(trajectory_id, submap_index) ==
           std::forward_as_tuple(other.trajectory_id, other.submap_index);
  }

  bool operator!=(const SubmapId& other) const
  {
    return !operator==(other);
  }

  bool operator<(const SubmapId& other) const
  {
    return std::forward_as_tuple(trajectory_id, submap_index) <
           std::forward_as_tuple(other.trajectory_id, other.submap_index);
  }
};

class SubOctoMaps
{
public:
  SubOctoMaps(float resolution);
  std::vector<cartographer_ros_msgs::SubmapEntry>
  handleSubmapList(cartographer_ros_msgs::SubmapList::ConstPtr submapList);

  void handleSubmapQueryResult(cartographer_ros_msgs::SubmapCloudQuery& queryResult,
                               cartographer_ros_msgs::SubmapEntry& submapEntry);
  bool rayCollision(Eigen::Vector3d& origin, Eigen::Vector3d& direction, Eigen::Vector3d& end, float maxRange = -1.f);
  bool has_active_submaps();
  sensor_msgs::PointCloud2 activeMaps(std::string frame_id);
  float resolution()
  {
    return resolution_;
  }

private:
  std::map<SubmapId, std::shared_ptr<SubOctoMap>> active_submaps_;
  std::mutex mutex_;
  float resolution_;
};  // class SubOctoMaps
}  // namespace obstacle_detection

#endif  // OBSTACLE_DETECTION_SUB_OCTO_MAPS_H_