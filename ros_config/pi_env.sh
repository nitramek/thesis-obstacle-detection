#!/usr/bin/env bash

#this is a env.sh file for raspberry pi which has i2c_imu tools installed in catkin_ws

#set ip of the one who is sshing to me, for now
MASTER_IP=`echo $SSH_CLIENT | awk '{ print $1}'`
PI_IP=`hostname -I | awk '{ print $1}'`

echo $MASTER_IP
echo $PI_IP
export ROS_IP=${PI_IP}
export ROS_MASTER_URI=http://${MASTER_IP}:11311
sudo ntpdate ${MASTER_IP}
source /home/pi/catkin_ws/devel/setup.bash

exec "$@"


