#include <iostream>
#include "ros/ros.h"
#include "tf2_ros/transform_listener.h"

#include "obstacle_detection/node.h"

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "obstacle_detection_node");
  ros::NodeHandle n, private_n("~");

  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);
  obstacle_detection::Node node(n, private_n, &tfBuffer);

  ros::spin();

  return 0;
}