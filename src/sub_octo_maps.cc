#include <algorithm>

#include "obstacle_detection/conversions.h"
#include "obstacle_detection/sub_octo_maps.h"
#include "pcl_ros/transforms.h"
#include "pcl/common/transforms.h"
#include "ros/console.h"


#define LAST_SUBMAPS_COUNT  2
namespace obstacle_detection
{
SubOctoMaps::SubOctoMaps(float resolution) : resolution_(resolution)
{
}
bool SubOctoMaps::has_active_submaps()
{
  std::lock_guard<std::mutex> lock(mutex_);
  return !active_submaps_.empty();
}
sensor_msgs::PointCloud2 SubOctoMaps::activeMaps(std::string frame_id)
{
  std::lock_guard<std::mutex> lock(mutex_);

  pcl::PointCloud<pcl::PointXYZI>::Ptr activeMap(new pcl::PointCloud<pcl::PointXYZI>);
  for (auto& submap : active_submaps_)
  {
    auto cloud = submap.second->mapCopy();
    if (cloud->empty())
      continue;
    auto submapPose = submap.second->pose();
    Eigen::Affine3d eigenTransform;
    tf::poseMsgToEigen(submapPose, eigenTransform);
    pcl::transformPointCloud(*cloud, *cloud, eigenTransform);
    *activeMap += *cloud;
  }
  sensor_msgs::PointCloud2 activePointCloud = obstacle_detection::pclToMsg(activeMap);
  activePointCloud.header.frame_id = frame_id;
  activePointCloud.header.stamp = ros::Time::now();
  return activePointCloud;
}
bool SubOctoMaps::rayCollision(Eigen::Vector3d& origin, Eigen::Vector3d& direction, Eigen::Vector3d& end,
                                      float maxRange)
{
  std::lock_guard<std::mutex> lock(mutex_);
  for (auto& submap : active_submaps_)
  {
    if (submap.second->rayCollision(origin, direction, end, maxRange))
    {
      return true;
    }
  }
}

std::vector<cartographer_ros_msgs::SubmapEntry>
SubOctoMaps::handleSubmapList(cartographer_ros_msgs::SubmapList::ConstPtr submapList)
{
  if (submapList->submap.empty())
  {
    return std::vector<cartographer_ros_msgs::SubmapEntry>();
  }

  int count = LAST_SUBMAPS_COUNT;
  std::vector<cartographer_ros_msgs::SubmapEntry> requests;
  std::vector<SubmapId> currentIds;
  for (auto rit = submapList->submap.rbegin(); rit != submapList->submap.rend() && count > 0; ++rit)
  {
    const cartographer_ros_msgs::SubmapEntry& entry = *rit;
    SubmapId id{ entry.trajectory_id, entry.submap_index };
    currentIds.push_back(id);
    requests.push_back(entry);
    if (active_submaps_.count(id) > 0)
    {
      active_submaps_[id]->set_pose(entry.pose);
    }
    else
    {
      active_submaps_[id] = std::make_shared<SubOctoMap>(entry.trajectory_id, entry.submap_index, resolution_, entry.pose);
    }
    count--;
  }
  std::vector<SubmapId> activeSubmapsIds;
  for (auto& idSubmapPair : active_submaps_)
  {
    activeSubmapsIds.push_back(idSubmapPair.first);
  }
  std::lock_guard<std::mutex> lock(mutex_);
  for (auto& submapId : activeSubmapsIds)
  {
    if (!(std::find(currentIds.begin(), currentIds.end(), submapId) != currentIds.end())){
      ROS_INFO("Removing submap %d", submapId.submap_index);
      active_submaps_.erase(submapId);
    }
  }
  return requests;
}
void SubOctoMaps::handleSubmapQueryResult(cartographer_ros_msgs::SubmapCloudQuery& queryResult,
                                          cartographer_ros_msgs::SubmapEntry& submapEntry)
{
  SubmapId id{ queryResult.request.trajectory_id, queryResult.request.submap_index };
  // ROS_INFO("Got Submap %d", submapEntry.submap_index);
  std::unique_lock<std::mutex> lock(mutex_);
  if (active_submaps_.count(id) > 0)
  {
    // ROS_INFO("Updating Submap %d", submapEntry.submap_index);
    // maybe submap list will delete it faster who knows, lets check
    auto subOctoMap = active_submaps_[id];
    lock.unlock();
    subOctoMap->update(queryResult.response.cloud, queryResult.response.submap_version, queryResult.response.finished);
  }
}
}  // namespace obstacle_detection