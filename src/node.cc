#include "obstacle_detection/node.h"
#include "geometry_msgs/TransformStamped.h"
#include "eigen_conversions/eigen_msg.h"

namespace obstacle_detection
{
void Node::submapsCallback(const cartographer_ros_msgs::SubmapList::ConstPtr& msg)
{
  // ROS_INFO("Submap callback");
  std::vector<cartographer_ros_msgs::SubmapEntry> requests = sub_octo_maps_->handleSubmapList(msg);
  for (auto& request : requests)
  {
      boost::shared_ptr<ResponseCallback> callbackPtr(new ResponseCallback(request, submap_cloud_query_client_, *sub_octo_maps_));
      ros::getGlobalCallbackQueue()->addCallback(callbackPtr);
  }
}

cartographer_ros_msgs::SubmapCloudQuery::Response Node::fetchSubmapResponse(const int trajectory_id,
                                                                            const int submap_index)
{
  cartographer_ros_msgs::SubmapCloudQuery scq;
  scq.request.trajectory_id = trajectory_id;
  scq.request.submap_index = submap_index;
  scq.request.min_probability = 0.8f;  // for now
  if (submap_cloud_query_client_.call(scq))
  {
    return scq.response;
  }
  else
  {
    ROS_WARN("Cartographer submap cloud query wasnt completed");
    return cartographer_ros_msgs::SubmapCloudQuery::Response();
  }
}

Node::Node(ros::NodeHandle& nodeHandle, ros::NodeHandle& private_node, const tf2_ros::Buffer* const tf_buffer)
{
  ROS_INFO("Initiating obstacle_detection_node");
  float resolution;
  private_node.param<float>("map_resolution", resolution, 0.10f);
  sub_octo_maps_ = std::make_shared<SubOctoMaps>(resolution);
  checker_ = std::make_shared<ObjectChecker>(*sub_octo_maps_, nodeHandle, private_node, tf_buffer);
  submaps_subscriber_ = nodeHandle.subscribe("submap_list", 1, &Node::submapsCallback, this);
  submap_cloud_query_client_ =
      nodeHandle.serviceClient<cartographer_ros_msgs::SubmapCloudQuery>("/submap_cloud_query", true);
  checker_->start();
}

Node::~Node()
{
}
}  // namespace obstacle_detection