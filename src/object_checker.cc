#include <functional>

#include "ros/ros.h"
#include "Eigen/Geometry"
#include "octomap_msgs/Octomap.h"

#include "geometry_msgs/Point.h"
#include "visualization_msgs/Marker.h"
#include "obstacle_detection/object_checker.h"
#include "obstacle_detection/ros_helpers.h"

namespace obstacle_detection
{
namespace
{
std::vector<std::pair<Eigen::Vector3d, Eigen::Vector3d>> generateRays(float x_size, float y_size, float z_size,
                                                                      float resolution)
{
  //(x_size / resolution) * (y_size / resolution)
  std::vector<std::pair<Eigen::Vector3d, Eigen::Vector3d>> rays;
  float x_front = x_size / 2;

  for (float y = -y_size / 2; y <= y_size / 2; y += resolution)
  {
    for (float z = -z_size + resolution; z <= 0; z += resolution)
    {
      rays.push_back(std::make_pair(Eigen::Vector3d(x_front, y, z), Eigen::Vector3d(1, 0, 0)));
    }
  }
  return rays;
}
visualization_msgs::Marker createMarker(Eigen::Vector3d& startArrow, int id, float resolution)
{
  visualization_msgs::Marker marker;
  marker.header.frame_id = "map";
  marker.header.stamp = ros::Time::now();
  marker.id = id;
  marker.action = visualization_msgs::Marker::ADD;
  marker.type = visualization_msgs::Marker::ARROW;
  marker.color.a = 1;
  marker.color.r = 1;
  marker.scale.x = resolution;
  marker.scale.y = resolution;
  marker.scale.z = resolution;
  marker.lifetime = ros::Duration(0);
  geometry_msgs::Point startArrowPoint;
  tf::pointEigenToMsg(startArrow, startArrowPoint);
  marker.points.resize(2);
  marker.points[0] = startArrowPoint;
  return marker;
}
visualization_msgs::MarkerArray generateMarkersFromRays(std::vector<std::pair<Eigen::Vector3d, Eigen::Vector3d>>& rays,
                                                        float resolution)
{
  visualization_msgs::MarkerArray markers;
  for (size_t i = 0; i < rays.size(); i++)
  {
    auto& ray = rays[i];
    markers.markers.push_back(createMarker(ray.first, i, resolution));
  }
  return markers;
}

void markMarker(visualization_msgs::Marker& arrow, bool collided, Eigen::Vector3d& arrowStart,
                Eigen::Vector3d& arrowEnd)
{
  arrow.color.g = collided ? 0 : 1;
  arrow.color.b = collided ? 0 : 1;
  geometry_msgs::Point arrowEndPoint, arrowStartPoint;
  tf::pointEigenToMsg(arrowEnd, arrowEndPoint);
  tf::pointEigenToMsg(arrowStart, arrowStartPoint);
  ROS_DEBUG("End ray in %d %f %f %f", collided ? 1 : 0, arrowEndPoint.x, arrowEndPoint.y, arrowEndPoint.z);
  arrow.points[0] = arrowStartPoint;
  arrow.points[1] = arrowEndPoint;
}
}  // namespace

void ObjectChecker::start()
{
  checker_thread_ = std::thread(&ObjectChecker::checkCurrentMap, this);
}
ObjectChecker::ObjectChecker(SubOctoMaps& maps, ros::NodeHandle& n, ros::NodeHandle& private_node,
                             const tf2_ros::Buffer* const tf_buffer)
  : maps_(maps), tf_helper_(n, tf_buffer)
{
  active_octomap_pub_ = n.advertise<sensor_msgs::PointCloud2>("active_octomap", 1);
  casted_rays_pub_ = n.advertise<visualization_msgs::MarkerArray>("casted_rays", 1);
  car_dimension_ = obstacle_detection::getCarDimension();
  private_node.param<float>("ray_max_range", max_range_, 1.f);
  private_node.param<float>("detection_rate_hz", detection_rate_hz_, 10.f);
  ROS_INFO("Waiting for first transform");
  car_position_transform_ = tf_helper_.lookupTransform("base_link", "map", 100);
  ROS_INFO("Got first transform");
}
void ObjectChecker::registerTransformForOctomap(SubOctoMap& octomap)
{
  geometry_msgs::TransformStamped transformToOctomap =
      obstacle_detection::createTransformStampedFromPose(octomap.pose(), "map", "active_octomap_frame");
  tf_helper_.registerTransform(0, transformToOctomap);
}
void ObjectChecker::checkCurrentMap()
{
  ros::Rate loop_rate(detection_rate_hz_);  // 1 / 10 one publish per 0.1 second
  Eigen::Vector3d forward = Eigen::Vector3d(1, 0, 0);
  rays_ = generateRays(car_dimension_[0], car_dimension_[1], car_dimension_[2], maps_.resolution());
  ray_markers_ = generateMarkersFromRays(rays_, maps_.resolution());
  ROS_INFO("Starting detection");
  int tf_counter = 0, map_pub_counter = 0;
  while (ros::ok())
  {
    if (tf_counter == 4)
    {
      Eigen::Affine3d oldTransform = car_position_transform_;
      car_position_transform_ = tf_helper_.lookupTransform("base_link", "map");
      Eigen::Vector3d movingDirection = car_position_transform_.translation() - oldTransform.translation();
      Eigen::Vector3d movingDirectionStaticFrame = oldTransform.rotation().inverse() * movingDirection;
      if (movingDirectionStaticFrame.x() != 0)
      {
        forward.x() = movingDirectionStaticFrame.x();
        forward.normalize();
      }
      tf_counter = 0;
    }
    else
    {
      car_position_transform_ = tf_helper_.lookupTransform("base_link", "map");
    }
    for (size_t i = 0; i < rays_.size(); i++)
    {
      auto& startEndPair = rays_[i];
      Eigen::Vector3d rayStartOrig = startEndPair.first;
      if (forward.x() < 0)
      {
        rayStartOrig.x() -= car_dimension_[0];
      }
      Eigen::Vector3d rayStart = car_position_transform_ * rayStartOrig;
      Eigen::Vector3d rayDirection = car_position_transform_.rotation() * forward;
      rayDirection.normalize();
      Eigen::Vector3d end;
      bool collided = maps_.rayCollision(rayStart, rayDirection, end, max_range_);
      if (collided)
      {
        ROS_INFO("Collided on in submap %.2f, %.2f %.2f", end.x(), end.y(), end.z());
      }
      Eigen::Vector3d arrowEnd = rayStart + rayDirection * max_range_;
      markMarker(ray_markers_.markers[i], collided, rayStart, arrowEnd);
    }
    LAZY_PUBLISH(casted_rays_pub_, ray_markers_);
    if (map_pub_counter == 10)
    {
      map_pub_counter = 0;
      LAZY_PUBLISH(active_octomap_pub_, maps_.activeMaps("map"));
    }

    tf_counter++;
    map_pub_counter++;
    loop_rate.sleep();
  }
}
ObjectChecker::~ObjectChecker()
{
  if (checker_thread_.joinable())
  {
    checker_thread_.join();
  }
}
}  // namespace obstacle_detection
