#include <omp.h>

#include "obstacle_detection/conversions.h"
#include "pcl/io/pcd_io.h"
#include "ros/console.h"

#include "obstacle_detection/sub_octo_map.h"
#include "tf2_sensor_msgs/tf2_sensor_msgs.h"
#include "geometry_msgs/TransformStamped.h"

namespace obstacle_detection
{
SubOctoMap::SubOctoMap()
  : trajectory_id_(-1), submap_index_(-1), resolution_(0), finished_(false), version_(-1), map_data_(nullptr)
{
  ROS_ERROR("Empty constructor");
  ROS_ASSERT(false);
}

SubOctoMap::SubOctoMap(int trajectory_id, int submap_index, float resolution, geometry_msgs::Pose pose)
  : trajectory_id_(trajectory_id)
  , submap_index_(submap_index)
  , resolution_(resolution)
  , finished_(false)
  , version_(0)
  , map_octree_(new pcl::octree::OctreePointCloudSearch<pcl::PointXYZI>(resolution))
  , map_data_(new pcl::PointCloud<pcl::PointXYZI>)
{
  set_pose(pose);
  map_octree_->defineBoundingBox(0, 0, 0, 1.f, 1.f, 1.f);
}
void SubOctoMap::update(sensor_msgs::PointCloud2& cloud_msg, int version, bool finished)
{
  if (!finished_)
  {
    version_ = version;
    finished_ = false;
    ROS_DEBUG("Submap %d version: %d %s", submap_index_, version_, finished_ ? "is finished" : "not finished");
    // tf2::doTransform(cloud_msg, cloud_msg, transform_);
    map_data_ = obstacle_detection::msgToPcl(cloud_msg);
    map_octree_->setInputCloud(map_data_);
    map_octree_->addPointsFromInputCloud();
  }
}
pcl::PointCloud<pcl::PointXYZI>::Ptr SubOctoMap::mapCopy()
{
  std::lock_guard<std::mutex> lock(mutex_);
  if (map_data_->points.empty())
  {
    return pcl::PointCloud<pcl::PointXYZI>::Ptr(new pcl::PointCloud<pcl::PointXYZI>);
  }
  else
  {
      return map_data_->makeShared();
  }
}
bool SubOctoMap::rayCollision(Eigen::Vector3d& origin, Eigen::Vector3d& direction, Eigen::Vector3d& end, float maxRange)
{
  // have to transform points from global to static frame, because SLAM can change our submap pose
  std::lock_guard<std::mutex> lock(mutex_);
  Eigen::Vector3f origin_in_submap = (to_submap_transform_ * origin).cast<float>();
  Eigen::Vector3f direction_in_submap =
      (to_submap_transform_ * direction - to_submap_transform_.translation()).cast<float>();

  if (map_data_->points.empty())
  {
    return false;
  }

  std::vector<pcl::PointXYZI, Eigen::aligned_allocator<pcl::PointXYZI> > centers;
  map_octree_->getIntersectedVoxelCenters(origin_in_submap, direction_in_submap, centers, 1);
  if (!centers.empty())
  {
    pcl::PointXYZI endPoint = centers.at(0);
    auto endEigen = Eigen::Vector3d(endPoint.x, endPoint.y, endPoint.z);
    end = from_submap_transform_ * endEigen;
    float length = (end - origin).norm() - resolution_;
    // ROS_INFO("%f vs %f", length, maxRange);
    return length < maxRange;
  }
  else
  {
    return false;
  }
}
}  // namespace obstacle_detection