#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Imu
from copy import deepcopy
import rosbag
import sys

# Imu messages from Android project doesnt have correct headers, this little script will correct that
# usage: ./imu_android_fixer <bag_filename> <optional_topic_name>
# it will save the fixed bag in the same path as the original bag only with .fixed postfix
#

    
def main():    
    bag_filename = sys.argv[1]
    android_topic_name = len(sys.argv) > 2 if "/android/tango1/imu" else sys.argv[2]
    with rosbag.Bag(bag_filename + ".fixed", 'w') as outbag:
        for topic, msg, t in rosbag.Bag(bag_filename).read_messages():
        # This also replaces tf timestamps under the assumption 
        # that all transforms in the message share the same timestamp
            if topic == android_topic_name and msg.transforms:
                imu_corrected_msg = deepcopy(msg)
                outbag.write(topic, imu_corrected_msg, t)
            else:
                outbag.write(topic, msg, t)
        

    

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass